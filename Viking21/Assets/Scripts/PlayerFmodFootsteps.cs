﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;


public class PlayerFmodFootsteps : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string footstepEvent;
    vThirdPersonInput tpInput;
    FMOD.Studio.EventInstance footstepInstance;

    // Start is called before the first frame update
    void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
        
    }


    void footstep()
    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {

            footstepInstance = FMODUnity.RuntimeManager.CreateInstance(footstepEvent);
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(footstepInstance, gameObject.transform, gameObject.GetComponent<Rigidbody>());
            
            footstepInstance.start();
            footstepInstance.release();
        }
            
    }
}
